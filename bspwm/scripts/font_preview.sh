#!/usr/bin/sh

font_file=$(fc-list | awk -F: '{print $1}' | dmenu -i -h 22 -p "Font: ")
display "$font_file"

