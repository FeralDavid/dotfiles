#!/usr/bin/sh

rom_path=/mnt/nas/roms
library_path=/usr/lib/libretro

[[ ! $(mount | grep nas) ]] && mount /mnt/nas

console=$(echo -e "Nintendo\nSuper Nintendo\nSega Master System\nGenesis\nArcade" | dmenu -i -h 22 -p "Console: ")
case "$console" in
    "Nintendo")
        rom_path="${rom_path}/nes"
        library_path="${library_path}/nestopia_libretro.so"
        ;;
    "Super Nintendo")
        rom_path="${rom_path}/snes"
        library_path="${library_path}/snes9x_libretro.so"
        ;;
    "Sega Master System")
        rom_path="${rom_path}/sms"
        library_path="${library_path}/picodrive_libretro.so"
        ;;
    "Genesis")
        rom_path="${rom_path}/genesis"
        library_path="${library_path}/picodrive_libretro.so"
        ;;
    "Arcade")
        rom_path="${rom_path}/arcade"
        library_path="${library_path}/mame_libretro.so"
        ;;
    *)
        exit 1
        ;;
esac

chosen=$(ls -1 "$rom_path" | rg -i '\.(nes|smc|sfc|zip)$' | dmenu -i -h 22 -p "Game: ")
echo "ROM: ${rom_path}/${chosen}"
echo "Library: $library_path"
[[ -f "${rom_path}/${chosen}" ]] && retroarch -L "$library_path" "${rom_path}/${chosen}"

