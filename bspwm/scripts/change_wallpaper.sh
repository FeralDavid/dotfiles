#!/usr/bin/zsh

wallpaper="$HOME/Pictures/wallpapers/current.jpg"
if [[ ( -f "$1" ) && ( "$1:t:e" = "jpg" ) ]]; then
    ln -sf "$1" "$wallpaper"
fi
xwallpaper --no-randr --stretch "$wallpaper"

