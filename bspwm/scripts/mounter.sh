#!/usr/bin/sh

mount_notification() {
    if [ $1 = 0 ]; then
        notify-send --app-name=mount --urgency=normal "Mount" "Successfully mounted $2"
    else
        notify-send --app-name=mount --urgency=critical "Mount" "Failed to mount $2"
    fi
}

unmount_notification() {
    if [ $1 = 0 ]; then
        notify-send --app-name=umount --urgency=normal "Umount" "Successfully unmounted $2"
    else
        notify-send --app-name=umount --urgency=critical "Umount" "Failed to unmount $2"
    fi
}

mount_nas() {
    mount | grep '/mnt/nas'
    if [ $? = 0 ]; then
        umount /mnt/nas
        unmount_notification $? /mnt/nas
    else
        mount /mnt/nas
        mount_notification $? /mnt/nas
    fi
}

mount_usb() {
    mount | grep '/mnt/usbhd'
    if [ $? = 0 ]; then
        umount /mnt/usbhd
        umount_notification $? /mnt/usbhd
    else
        mount /mnt/usbhd
        mount_notification $? /mnt/usbhd
    fi
}

mount_phone() {
    local phone_count=$(simple-mtpfs -l | wc -l)
    if [ $phone_count = 0 ]; then
        notify-send --icon="~/.local/share/icons/failure_icon.png" "Phone Disconnected" "Phone is not connected"
        exit 1
    fi

    mount | grep '/mnt/phone'
    if [ $? = 0 ]; then
        fusermount /mnt/phone
        unmount_notification $? /mnt/phone
    else
        simple-mtpfs --device 1 /mnt/phone
        mount_notification $? /mnt/phone
    fi
}

my_share=$(echo -e "nas\nphone\nusb" | dmenu -p "Mount")
case $my_share in
    "nas")
        mount_nas
        ;;
    "phone")
        mount_phone
        ;;
    "usb")
        mount_usb
        ;;
esac

