#!/usr/bin/sh

tv_path=/mnt/nas/tv

[[ ! $(mount | grep nas) ]] && mount /mnt/nas

series_chosen=$(ls -1 $tv_path | dmenu -i -h 22 -p "TV Series: ")
[[ -z $series_chosen ]] && exit 0

season_chosen=$(ls -1 ${tv_path}/${series_chosen} | dmenu -i -h 22 -p "Season: ")
[[ -z $season_chosen ]] && exit 0

mpv ${tv_path}/${series_chosen}/${season_chosen}
