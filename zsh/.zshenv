append_path () {
    case ":$PATH:" in
        *:"$1":*)
            ;;
        *)
            PATH="${PATH:+$PATH:}$1"
    esac
}

generate_password() {
    cat /dev/urandom | tr -dc [:print:] | head -c 16 | xargs
}

scanimage() {
    command scanimage --resolution 75 --format=png --output-file="$HOME/Pictures/scanbox/$1" --progress
}

append_path $HOME/.local/bin
append_path $HOME/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/bin
append_path /opt/RustRover/bin
export PATH

export GROFF_NO_SGR=1
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export EDITOR=nvim
export BROWSER=firefox
export PAGER=less
export LESSHISTFILE=-
export CARGO_HOME=$HOME/.config/cargo

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_PICTURES_DIR="$HOME/Pictures"

export GRIM_DEFAULT_DIR="$HOME/Pictures/screenshots"

export NNN_BMS="p:$HOME/Projects;d:$HOME/Downloads;n:/mnt/nas;u:/mnt/usbhd"
export NNN_ARCHIVE="\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$"
export NNN_FIFO=/tmp/nnn.fifo

unset append_path
