bindkey -v
export KEYTIMEOUT=1

#Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}

zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}

zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

export HISTFILE=$HOME/.cache/zsh/histfile
export HISTSIZE=10000
export SAVEHIST=10000

setopt HIST_IGNORE_ALL_DUPS
setopt HIST_SAVE_NO_DUPS
setopt SHARE_HISTORY
setopt HIST_IGNORE_SPACE

setopt extendedglob
unsetopt beep nomatch
unsetopt extended_history

autoload -U zmv
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit -d ~/.cache/zsh/zcompdump
_comp_options+=(globdots)

# Fixes the backspace key in vi mode
bindkey "^?" backward-delete-char

alias ls='exa'
alias l.='exa -a'
alias ll='exa -lg --time-style=long-iso'
alias ll.='exa -alg --time-style=long-iso'
alias tree='tree -C'
alias nnn='nnn -QudUic'

alias .2='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'

alias top='btop'
alias vim='nvim'
alias du='dust'

alias pacman='pacman --color=auto'
alias paru='paru --color=auto'

alias upd='doas pacman -Syyu'
alias mirrors='doas reflector --verbose --country "United States" -n 10 -p http --sort rate --save /etc/pacman.d/mirrorlist'

alias za='zathura'

alias play='paplay'
alias rec='parec'

alias img2pdf='img2pdf --auto-orient --pagesize A4^T'

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

eval "$(starship init zsh)"

