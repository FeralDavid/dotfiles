#!/usr/bin/sh

movie_path=/mnt/nas/movies

[[ ! $(mount | grep 'mnt/nas') ]] && mount /mnt/nas

chosen=$(ls -1 "$movie_path" | rg -i '\.(mkv|avi)$' | dmenu "Movie")

[[ -f "${movie_path}/$chosen" ]] && mpv "$movie_path/$chosen"

