#!/usr/bin/sh

tv_path=/mnt/nas/tv

[[ ! $(mount | grep 'mnt/nas') ]] && mount /mnt/nas

series_chosen=$(ls -1 "$tv_path" | dmenu "TV Series")
[[ -z $series_chosen ]] && exit 0

season_chosen=$(ls -1 "${tv_path}/${series_chosen}" | dmenu "Season")
[[ -z "$season_chosen" ]] && exit 0

mpv "${tv_path}/${series_chosen}/${season_chosen}"
