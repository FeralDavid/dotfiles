#!/usr/bin/sh

cd $HOME/.password-store

choice=$(tree -fin --noreport . | grep '\.gpg' | sed 's/\.\/\(.*\)\.gpg/\1/g' | dmenu "Password")
pass -c "$choice"
