#!/usr/bin/zsh

for p in $(ps au | grep "$1" | grep -v 'grep' | awk '{print $2}'); do
    kill -9 $p
done
