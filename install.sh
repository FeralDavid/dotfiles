#!/usr/bin/sh

# Update mirrors
sudo pacman -S reflector
sudo reflector --verbose --country "United States" -n 10 -p http --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Syy

# Install packages
cat pacman_packages.list | xargs pacman -S

sudo usermod -a -G wheel,storage,optical,audio,video dave

cd
git clone https://aur.archlinux.org/paru.git
cd paru
sudo makepkg -si
cd ..
rm -Rf paru

# AUR Applications
cat paru_packages.list | xargs paru -S

sudo echo "permit :wheel" > /etc/doas.conf
sudo echo "permit nopass :wheel as root cmd /usr/bin/poweroff" >> /etc/doas.conf
sudo echo "permit nopass :wheel as root cmd /usr/bin/reboot" >> /etc/doas.conf

doas pacman -Rs sudo
doas ln -s /usr/bin/doas /usr/bin/sudo

