#!/usr/bin/sh

REPO_DIR=$HOME/Projects/dotfiles

echo "Copying new/updated files into repo..."

cp -uR $HOME/.config/hypr $REPO_DIR
cp -u $HOME/.config/alacritty/alacritty.toml $REPO_DIR/alacritty
cp -u $HOME/.config/dunst/dunstrc $REPO_DIR/dunst
cp -uR $HOME/.config/zsh $REPO_DIR
cp -u $HOME/.config/nvim/init.lua $REPO_DIR/nvim
cp -uR $HOME/.config/nvim/lua $REPO_DIR/nvim
cp -uR $HOME/.config/nvim/after $REPO_DIR/nvim
cp -uR $HOME/.config/waybar $REPO_DIR
cp -u $HOME/.config/starship.toml $REPO_DIR
cp -uR $HOME/.local/bin $REPO_DIR/local
