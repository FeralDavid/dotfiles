require('lazy').setup({
    {'dracula/vim', name = 'dracula'},
    {'williamboman/mason.nvim'},
    {'williamboman/mason-lspconfig.nvim'},
    {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v4.x',
    },
    {
        'neovim/nvim-lspconfig',
        dependencies = {
            {'hrsh7th/cmp-nvim-lsp'}
        }
    },
    {
        'hrsh7th/nvim-cmp',
        dependencies = {
            {
                'L3MON4D3/LuaSnip',
                version = "v2.*",
                build = "make install_jsregexp"
            }
        }
    },
    {'vim-airline/vim-airline'},
    {'hrsh7th/cmp-path'},
    {'hrsh7th/cmp-cmdline'},
    {'simrat39/rust-tools.nvim'},
    {'mfussenegger/nvim-dap'},
    {'nvim-lua/popup.nvim'},
    {'nvim-telescope/telescope.nvim'},
    {
        "nvim-telescope/telescope-symbols.nvim",
        dependencies = { {'nvim-lua/plenary.nvim'} }
    },
    {
        "nvim-treesitter/nvim-treesitter",
        run = ':TSUpdate'
    },
    {'nvim-tree/nvim-web-devicons'}
})

