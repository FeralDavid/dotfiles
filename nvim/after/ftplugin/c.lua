local cwd = vim.fn.getcwd()
local found = string.find(cwd:reverse(), "/") - 1
local binary_name = string.sub(cwd, -found)
vim.keymap.set("n", '<F10>', ':!make && ./' .. binary_name .. '<CR>')
